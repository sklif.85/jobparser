from argparse import ArgumentParser, RawTextHelpFormatter
from workua import workua

source_parser = {
    "workua": workua
}


def main():
    source_parser[args.s](args)


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('-s', metavar='source type', help='name of job site')
    parser.add_argument('-v', metavar='vacancy type', help='name of vacancy on job site')
    parser.add_argument('-c', metavar='city', help='name of the city')
    parser.add_argument('-o', metavar='output', help='output type')
    args = parser.parse_args()
    if args.s is None:
        print("I need some sorce to parse...exit")
        exit(1)
    main()
