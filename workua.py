import requests
import re
import pandas as pd
import unicodecsv as csv
import copy

RESULT = []


# base_url = "https://www.work.ua/ua/jobs-{}-{}/?advs=1&sel_zan=74&page={}"
# base_vacancy_url = "https://www.work.ua/ua/jobs/{}/"


def workua(args):
    print(f"Run [{args.s}] parser if city [{args.c}] on vacancy [{args.v}]")

    page = 1
    while True:
        url = f"https://www.work.ua/ua/jobs-{args.c}-{args.v}/?advs=1&sel_zan=74&page={str(page)}"
        r = requests.get(url)
        vacancy_ids_raw = re.findall(r'<a name="\d+"', r.text)
        if len(vacancy_ids_raw) == 0:
            break

        vacancy_ids = [val.split("\"")[1] for val in vacancy_ids_raw]

        for vacancy_id in vacancy_ids:
            r = requests.get(f"https://www.work.ua/ua/jobs/{vacancy_id}/")
            # print(r.url)
            lines = r.text.split("\n")

            title = None
            salary = None
            company = None
            description = None
            for i, line in enumerate(lines):
                if 'id="h1-name">' in line:
                    title = line

                if 'class="text-muted text-muted-print"' in line:
                    salary = line

                if '"og:title"' in line:
                    company = line

                tmp = ''
                if 'Опис вакансії' in line:
                    j = i + 1

                    try:
                        while True:
                            if "Надіслати резюме" in lines[j]:
                                break
                            l = copy.copy(lines[j])
                            tmp += l
                            j += 1
                        description = tmp
                    except Exception as e:
                        print(e)

                if title is not None and salary is not None and company is not None and description is not None:
                    break

            clear_data(title, salary, company, description)

        page += 1

    if args.o == "csv":
        save_to_csv(args)
    elif args.o == "xlsx":
        save_to_xlsx(args)

    print("END parse with success!")


def clear_data(title, salary, company, description):
    try:
        title = re.findall(r'>\w+', title)[0][1:]
    except Exception as e:
        pass

    s = None
    if salary is not None:
        try:
            s = re.findall(r'>\d+', salary)
            if len(s) != 0:
                s = s[0][1:]
            else:
                try:
                    s = re.findall(r'>\s+\d+', salary)[0][1:]
                except Exception as e:
                    pass
        except Exception as e:
            pass
    if type(s) is list:
        s = None

    company = company.split(", ")
    for c in company:
        if "компанія" in c:
            company = c
            break

    if type(company) is list:
        company = company[0]
    company = company.replace('">', '')
    company = company.replace('"', '')
    company = company.replace('>', '')

    description = description.replace('<p>', '')
    description = description.replace('</p>', '')
    description = description.replace('<b>', '')
    description = description.replace('</b>', '')
    description = description.replace('<ul>', '')
    description = description.replace('<h3>', '')
    description = description.replace('</h3>', '')
    description = description.replace('<li>', '')
    description = description.replace('</li>', '')
    description = description.replace('\n', ' ')
    description = description.replace('</ul>', '')
    description = description.replace('<ul>', '')
    description = description.replace('<div class="form-group hidden-print">', '')

    res = {}
    res['title'] = title
    res['salary'] = s
    res['company'] = company
    res['description'] = description

    RESULT.append(res)


def save_to_csv(args):
    args.v = args.v.replace("+", "_")
    file = f"{args.s}_{args.c}_{args.v}.csv"

    keys = RESULT[0].keys()
    with open(file, 'wb') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(RESULT)


def save_to_xlsx(args):
    args.v = args.v.replace("+", "_")
    file = f"{args.s}_{args.c}_{args.v}.xlsx"

    df = pd.DataFrame(RESULT)
    writer = pd.ExcelWriter(file, engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Sheet1')
    writer.save()
